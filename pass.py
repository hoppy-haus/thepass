import socket
import string
from threading import Thread
import re

# First server
HOST="irc.orderofthetilde.net"

#Second server
HOST2="irc.goat.chat"

#Self explanitory
PORT=6667
NICK="ThePass"
IDENT="Bridgebot"
REALNAME="Bridgebot"
readbuffer=""

S1CHAN = '#gamebridge'
S2CHAN = '#gaming'

# Initialize the socket for the first server, and send the commands to start 
sb=socket.socket( )
sb.connect((HOST, PORT))
sb.send("NICK %s\r\n" % NICK)
sb.send("USER %s %s bla :%s\r\n" % (IDENT, HOST, REALNAME))

# Change '#gamebridge' to the channel to mirror on server 1
sb.send('JOIN ' + S1CHAN + '\r\n')

# Initialize the socket for the second server, and send the commands to start
sg=socket.socket( )
sg.connect((HOST2, PORT))
sg.send("NICK %s\r\n" % NICK)
sg.send("USER %s %s bla :%s\r\n" % (IDENT, HOST, REALNAME))

# Change '#gamebridge' to the channel to mirror on server 2
sg.send('JOIN ' + S2CHAN + '\r\n')

# Initialize the def for the loop that server 1 will be connected to
def bepho():
    readbuffer=''
    #loop
    while 1:
        # Recieve and initially process the recieved text
        readbuffer=readbuffer+ sb.recv(1024)
        temp=string.split(readbuffer, "\n")
        readbuffer=temp.pop( )
        
        #start text processing
        for line in temp:
            # feed is the raw string, line has each word separated in a list
            feed=string.rstrip(line)
            line=string.rstrip(line)
            line=string.split(line)
            # Print processed text to console
            print line
            try:
                # Detect someone saying something
                if any("PRIVMSG" in s for s in line):
                    # split the raw line into a list of 3 lines, with the final one being the actual message
                    m = feed.split(':',2)[2] + "\r\n"
                    # use (probably bad) reget to extract the username of the person speaking
                    se = re.findall(':(.*?)!', line[0])[0]
                    # send to the other server what was said with the format of "[USER] | [MESSAGE]"
                    sg.send('PRIVMSG ' + S2CHAN + ' :' + se + ' | ' + m)
            except IndexError:
                pass
            if (line[0]=="PING"):
                sb.send("PONG %s\r\n" % line[1])
                sb.send('JOIN ' + S1CHAN + '\r\n')

# Almost the same code for the first server, just reversed for the second server
def goet():
    readbuffer=''
    while 1:
        readbuffer=readbuffer+sg.recv(1024)
        temp=string.split(readbuffer, "\n")
        readbuffer=temp.pop( )

        for line in temp:
            feed=string.rstrip(line)
            line=string.rstrip(line)
            line=string.split(line)
            print line
            try:
                print line[3]
                print feed
                if any("PRIVMSG" in s for s in line):
                    m = feed.split(':',2)[2] + "\r\n"
                    se = re.findall(':(.*?)!', line[0])[0]
                    sb.send('PRIVMSG ' + S1CHAN + ' :' + se + ' | ' + m)
            except IndexError:
                pass
            if (line[0]=="PING"):
                sg.send("PONG %s\r\n" % line[1])
                sg.send('JOIN ' + S2CHAN + '\r\n')


if __name__ == '__main__':
    # Initialize threads and start them
    bapho = Thread(target=bepho)
    goat = Thread(target=goet)
    bapho.start()
    goat.start()